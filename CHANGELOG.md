# Changelog

This changelog is based on [Keep a Changelog 1.0.0](https://keepachangelog.com/en/1.0.0).

This project adheres to [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0).

## [0.2.0] - 2024-04-14

### Changed

- Renamed methods `do*` to `*Internal` in `*Support` classes.

## [0.1.0] - 2024-04-06

### Added

- Created `Initializable`
- Created `InitializableOperations`
- Created `Startable`
- Created `StartableOperations`
- Created `LifeCycleSupport`
- Created `InitializableSupport`
- Created `StartableSupport`
- Created `InitializableStartableSupport`
