/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.lifecycle;

import java.util.concurrent.locks.Lock;

/**
 * {@link LifeCycleSupport} for both the {@link Initializable} and {@link Startable} life cycles.
 * <p>
 * This class is thread-safe.
 * </p>
 *
 * @author Laurent Pireyn
 */
public class InitializableStartableSupport extends InitializableSupport implements Startable {
    private boolean started;

    @Override
    public final boolean isStarted() {
        final Lock readLock = lock.readLock();
        readLock.lock();
        try {
            return started;
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public final void start() throws Exception {
        final Lock writeLock = lock.writeLock();
        writeLock.lock();
        try {
            startLocked();
        } finally {
            writeLock.unlock();
        }
    }

    private void startLocked() throws Exception {
        if (!started) {
            initializeLocked();
            startInternal();
            started = true;
        }
    }

    protected void startInternal() throws Exception {}

    @Override
    public final void stop() throws Exception {
        final Lock writeLock = lock.writeLock();
        writeLock.lock();
        try {
            stopLocked();
        } finally {
            writeLock.unlock();
        }
    }

    private void stopLocked() throws Exception {
        if (started) {
            try {
                stopInternal();
            } finally {
                started = false;
            }
        }
    }

    protected void stopInternal() throws Exception {}

    @Override
    void destroyLockedInternal() throws Exception {
        stopLocked();
        super.destroyLockedInternal();
    }
}
