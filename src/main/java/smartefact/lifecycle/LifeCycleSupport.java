/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.lifecycle;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Support for life cycle.
 * <p>
 * Subclasses of this class are typically defined as inner classes
 * that hold the life cycle-related state
 * and implement the related operations.
 * </p>
 * <p>
 * This is a sealed class.
 * </p>
 * <p>
 * This class and its subclasses are thread-safe.
 * </p>
 *
 * @author Laurent Pireyn
 * @see InitializableSupport
 * @see StartableSupport
 * @see InitializableStartableSupport
 */
public abstract class LifeCycleSupport {
    final ReadWriteLock lock = new ReentrantReadWriteLock();

    LifeCycleSupport() {}
}
