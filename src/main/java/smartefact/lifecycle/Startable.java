/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.lifecycle;

/**
 * Object that can be started and stopped.
 * <p>Rules:</p>
 * <ul>
 *     <li>A startable object can be started and stopped any number of times.</li>
 *     <li>Starting an already started object does nothing.</li>
 *     <li>Stopping an object that is not started does nothing.</li>
 * </ul>
 *
 * @author Laurent Pireyn
 */
public interface Startable extends StartableOperations {
    /**
     * Returns whether this object is started.
     *
     * @return {@code true} if this object is started,
     * {@code false} otherwise
     */
    boolean isStarted();
}
