/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.lifecycle;

import java.util.concurrent.TimeUnit;
import org.jetbrains.annotations.NotNull;

/**
 * Object that can be initialized and destroyed.
 * <p>Rules:</p>
 * <ul>
 *     <li>An initializable object can be initialized and destroyed only once.</li>
 *     <li>Initializing an already initialized object does nothing.</li>
 *     <li>Initializing a destroyed object throws an {@link IllegalStateException}.</li>
 *     <li>Destroying an already destroyed object does nothing.</li>
 *     <li>Destroying an object that is not initialized should not throw any exception.
 *     The object must be considered as destroyed nonetheless.
 *     </li>
 * </ul>
 *
 * @author Laurent Pireyn
 */
public interface Initializable extends InitializableOperations {
    /**
     * Returns whether this object is initialized.
     *
     * @return {@code true} if this object is initialized,
     * {@code false} otherwise
     */
    boolean isInitialized();

    /**
     * Returns whether this object is destroyed.
     *
     * @return {@code true} if this object is destroyed,
     * {@code false} otherwise
     */
    boolean isDestroyed();

    /**
     * Waits indefinitely for this object to be destroyed.
     *
     * @throws InterruptedException if the current thread is interrupted while waiting
     */
    default void awaitDestroyed() throws InterruptedException {
        if (!awaitDestroyed(Long.MAX_VALUE, TimeUnit.DAYS)) {
            throw new AssertionError();
        }
    }

    /**
     * Waits for this object to be destroyed, with a timeout.
     *
     * @param timeout the maximum amount of time to wait (must not be negative)
     * @param unit the units of {@code timeout} (must not be {@code null})
     * @return {@code true} if this object is destroyed,
     * {@code false} if the timeout expired before this object is destroyed
     * @throws InterruptedException if the current thread is interrupted while waiting
     */
    boolean awaitDestroyed(long timeout, @NotNull TimeUnit unit) throws InterruptedException;
}
