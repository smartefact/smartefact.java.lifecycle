/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.lifecycle;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import org.jetbrains.annotations.NotNull;
import static smartefact.util.Preconditions.requireNotNull;

/**
 * {@link LifeCycleSupport} for the {@link Initializable} life cycle.
 * <p>
 * This class is thread-safe.
 * </p>
 *
 * @author Laurent Pireyn
 */
public class InitializableSupport extends LifeCycleSupport implements Initializable {
    private boolean initialized;
    private boolean destroyed;
    private final Condition destroyedCondition = lock.writeLock().newCondition();

    @Override
    public final boolean isInitialized() {
        final Lock readLock = lock.readLock();
        readLock.lock();
        try {
            return initialized;
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public final boolean isDestroyed() {
        final Lock readLock = lock.readLock();
        readLock.lock();
        try {
            return destroyed;
        } finally {
            readLock.unlock();
        }
    }

    @Override
    public final void initialize() throws Exception {
        final Lock writeLock = lock.writeLock();
        writeLock.lock();
        try {
            initializeLocked();
        } finally {
            writeLock.unlock();
        }
    }

    // Not private as it is called in InitializableStartableSupport
    final void initializeLocked() throws Exception {
        if (destroyed) {
            throw new IllegalStateException("This object is already destroyed and cannot be initialized");
        }
        if (!initialized) {
            initializeInternal();
            initialized = true;
        }
    }

    protected void initializeInternal() throws Exception {}

    @Override
    public final void destroy() throws Exception {
        final Lock writeLock = lock.writeLock();
        writeLock.lock();
        try {
            destroyLocked();
        } finally {
            writeLock.unlock();
        }
    }

    private void destroyLocked() throws Exception {
        if (!destroyed) {
            try {
                if (initialized) {
                    destroyLockedInternal();
                }
            } finally {
                destroyed = true;
                destroyedCondition.signalAll();
            }
        }
    }

    // Extracted as it is overridden in InitializableStartableSupport
    void destroyLockedInternal() throws Exception {
        destroyInternal();
    }

    protected void destroyInternal() throws Exception {}

    @Override
    public final void awaitDestroyed() throws InterruptedException {
        final Lock writeLock = lock.writeLock();
        writeLock.lock();
        try {
            awaitDestroyLocked();
        } finally {
            writeLock.unlock();
        }
    }

    private void awaitDestroyLocked() throws InterruptedException {
        while (!destroyed) {
            destroyedCondition.await();
        }
    }

    @Override
    public final boolean awaitDestroyed(long timeout, @NotNull TimeUnit unit) throws InterruptedException {
        requireNotNull(unit);
        final Lock writeLock = lock.writeLock();
        writeLock.lock();
        try {
            return awaitDestroyLocked(timeout, unit);
        } finally {
            writeLock.unlock();
        }
    }

    private boolean awaitDestroyLocked(long timeout, @NotNull TimeUnit unit) throws InterruptedException {
        while (!destroyed) {
            if (destroyedCondition.await(timeout, unit)) {
                return true;
            }
        }
        return false;
    }
}
