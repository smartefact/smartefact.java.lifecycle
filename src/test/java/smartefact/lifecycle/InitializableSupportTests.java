/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.lifecycle;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

class InitializableSupportTests {
    TestInitializableSupport support = new TestInitializableSupport();

    @Test
    void testInitialState() {
        assertFalse(support.isInitialized());
        assertFalse(support.isDestroyed());
    }

    @Test
    void testInitialize() throws Exception {
        support.initialize();
        assertTrue(support.isInitialized());
        assertFalse(support.isDestroyed());
    }

    @Test
    void testInitializeTwice() throws Exception {
        support.initialize();
        support.throwException = true;
        assertDoesNotThrow(() -> support.initialize());
    }

    @Test
    void testInitializeError() throws Exception {
        support.throwException = true;
        assertThrows(Exception.class, () -> support.initialize());
        assertFalse(support.isInitialized());
    }

    @Test
    void testDestroy() throws Exception {
        support.initialize();
        support.destroy();
        assertTrue(support.isInitialized());
        assertTrue(support.isDestroyed());
    }

    @Test
    void testDestroyNotInitialized() throws Exception {
        support.throwException = true;
        assertDoesNotThrow(() -> support.destroy());
    }

    @Test
    void testDestroyTwice() throws Exception {
        support.initialize();
        support.destroy();
        support.throwException = true;
        assertDoesNotThrow(() -> support.destroy());
    }

    @Test
    void testDestroyError() throws Exception {
        support.initialize();
        support.throwException = true;
        assertThrows(Exception.class, () -> support.destroy());
        assertTrue(support.isInitialized());
        assertTrue(support.isDestroyed());
    }
}
