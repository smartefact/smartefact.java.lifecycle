/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.lifecycle;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

class StartableSupportTests {
    TestStartableSupport support = new TestStartableSupport();

    @Test
    void testInitialState() {
        assertFalse(support.isStarted());
    }

    @Test
    void testStart() throws Exception {
        support.start();
        assertTrue(support.isStarted());
    }

    @Test
    void testStartTwice() throws Exception {
        support.start();
        support.throwException = true;
        assertDoesNotThrow(() -> support.start());
    }

    @Test
    void testStartError() throws Exception {
        support.throwException = true;
        assertThrows(Exception.class, () -> support.start());
        assertFalse(support.isStarted());
    }

    @Test
    void testStop() throws Exception {
        support.start();
        support.stop();
        assertFalse(support.isStarted());
    }

    @Test
    void testStopNotStarted() throws Exception {
        support.throwException = true;
        assertDoesNotThrow(() -> support.stop());
    }

    @Test
    void testStopTwice() throws Exception {
        support.stop();
        support.throwException = true;
        assertDoesNotThrow(() -> support.stop());
    }

    @Test
    void testStopError() throws Exception {
        support.start();
        support.throwException = true;
        assertThrows(Exception.class, () -> support.stop());
        assertFalse(support.isStarted());
    }
}
