[![Latest release](https://gitlab.com/smartefact/smartefact.java.lifecycle/-/badges/release.svg?key_text=Latest%20release&key_width=100)](https://gitlab.com/smartefact/smartefact.java.lifecycle/-/releases)
[![Pipeline status](https://gitlab.com/smartefact/smartefact.java.lifecycle/badges/main/pipeline.svg?key_text=Pipeline%20status&key_width=100)](https://gitlab.com/smartefact/smartefact.java.lifecycle/-/commits/main)
[![Coverage](https://gitlab.com/smartefact/smartefact.java.lifecycle/badges/main/coverage.svg?key_text=Coverage&key_width=100)](https://gitlab.com/smartefact/smartefact.java.lifecycle/-/commits/main)

# smartefact.java.lifecycle

Java library that defines a few common *life cycles*.

[Javadoc](https://smartefact.gitlab.io/smartefact.java.lifecycle)

## Changelog

See [CHANGELOG](CHANGELOG.md).

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md).

## License

This project is licensed under the [Apache License 2.0](LICENSE).
